import React, {Component} from 'react'
import{
    CheckBox,
    Body,
    Text,
    ListItem,
    Container,
    Button,
    Icon,
    Item,
    Input,
    View,
    ListView,
    Left,
    List,
    Content
} from 'native-base'


class MenuListComponents extends Component {
    constructor(props){
        super(props)
        this.state={
            MenuList:[],
                 
                       
        }

      
        this.renderMenuList=this.renderMenuList.bind(this);
    }

    componentWillReceiveProps(nextProps){
        this.setState({
            MenuList:nextProps.MenuList,
         
        })
    }

    renderMenuList(){
        return (
            console.log(this.state.MenuList),
            this.state.MenuList && this.state.MenuList.length > 0 ?
                this.state.MenuList.map((obj, i) => (
                    <ListItem key={i}>
                    <Text>{obj.name}</Text>
                    <Left>
                    <Text>RM{obj.price}</Text>
                    </Left>
                    </ListItem>

                ))

                : <Text>Empty Menu List</Text>

        )
    }

    render(){
        return(
            <Container>
                 <Text>Menu List</Text>
                 <Content>
                     <List>
                {this.renderMenuList()}
                </List>
                </Content>
            </Container>
        )   

    }

} 

export default MenuListComponents