import React, { Component } from 'react'
import {
    CheckBox,
    Body,
    Text,
    ListItem,
    Container,
    Button,
    Icon,
    Item,
    Input,
    View,
    Right
} from 'native-base'

class OrderListComponents extends Component {
    constructor(props) {
        super(props)
        this.state = {
            OrderList: [],
            PreparingList: [],
            ServedList:[],
            key:null
        }

        this.renderPreparingList = this.renderPreparingList.bind(this);
        this.renderServedList = this.renderServedList.bind(this);
        this.renderOrderList = this.renderOrderList.bind(this);
        // this.changeStatus=this.changeStatus.bind(this);
    }

    //------------keep update the data-------------//
    componentWillReceiveProps(nextProps) {
        this.setState({
            PreparingList: nextProps.PreparingList,
            ServedList: nextProps.ServedList,
            OrderList: nextProps.OrderList,
        })
    }

    // changeStatus(status,key)
    // {
    // this.setState({
    //     status:'Preparing',
    //     key:key
    //     })
    // }




    renderOrderList() {
        return (
            this.state.OrderList && this.state.OrderList.length > 0 ?
                this.state.OrderList.map((obj, i) => (
                    <ListItem key={i}>
                     <Text>{obj.name}</Text>
                        <Right>
                        <CheckBox checked={obj.status} onPress={() => this.props.changeStatus(obj._key, obj.status)} />
                        </Right>
                       

                    </ListItem>

                ))

                : <Text>Empty Order List</Text>

        )
    }


    renderPreparingList() {
        return (
            //--------to Container-------//
            this.state.PreparingList && this.state.PreparingList.length > 0 ?
                this.state.PreparingList.map((obj, i) => (
                    <ListItem key={i}>
                        
                        <Text>{obj.name}</Text>
                        <Right>
                        <CheckBox checked={obj.status} onPress={() => this.props.changeStatus1(obj._key, obj.status)} />
                        </Right>    
                    </ListItem>

                ))

                : <Text>Empty Preparing List</Text>

        )
    }

    renderServedList() {
        return (
            this.state.ServedList && this.state.ServedList.length > 0 ?
                this.state.ServedList.map((obj, i) => (
                    <ListItem key={i}>
                    <Text>{obj.name}</Text>
                    </ListItem>

                ))

                : <Text>Empty Served List</Text>

        )
    }

    render() {
        return (
            <Container>
                {/* /----------display--------- */}
                
                <Text>Order List</Text>
                {this.renderOrderList()}
                <Text>Preparing List</Text>
                {this.renderPreparingList()}
                <Text>Served List</Text>
                {this.renderServedList()}
            </Container>
        )

    }

}

export default OrderListComponents