// @flow
import React from "react";
import { StackNavigator, DrawerNavigator } from "react-navigation";
import { Root } from "native-base";
// import Login from "./container/LoginContainer";
// import Home from "./container/HomeContainer";
// import BlankPage from "./container/BlankPageContainer";
// import Sidebar' from "./container/SidebarContainer";
import MenuPage from './Modules/Kitchen/MenuPage';
import Main  from './Modules/Kitchen/container/OrderListContainer'

// const Drawer = DrawerNavigator(
// 	{
// 		// OrderList: { screen: OrderList },
// 	},
// 	{
// 		initialRouteName: "OrderList",
// 		contentComponent: props => <Sidebar {...props} />,
// 	}
// );

const App = StackNavigator(
	{
		// Login: { screen: Login },
		// BlankPage: { screen: BlankPage },
		// Drawer: { screen: Drawer },
		MenuPage:{screen: MenuPage},
		Main:{screen:Main}
	
	},
	{
		initialRouteName: "MenuPage",
		headerMode: "none",
	}
);

export default () => (
	<Root>
		<App />
	</Root>
);
