import * as React from "react";
import Main from "../../stories/screens/OrderList";
export interface Props {
	navigation: any,
}
export interface State {}
export default class MainContainer extends React.Component<Props, State> {
	render() {
		return <Main navigation={this.props.navigation} />;
	}
}
