import {StyleSheet}from "react-native";
const styles =StyleSheet.create({
    container:{
        flex:1,
    },
    header:{
        backgroundColor:'#87ceeb',
        justifyContent:'center', 
        alignItems:'center',
    },
    headerText:{
        color:'white',
        fontSize:30,
        padding:26,
        fontWeight:'bold',
    
    },
    buttonContainer:{
        backgroundColor: '#2980b9',
        width:80,
        height:20,
        alignItems:'center',
        position:'absolute',
        top:40,
        left:-1,
    },
    buttonText:{
        color:'white',
    },
    
    });

    export default styles;