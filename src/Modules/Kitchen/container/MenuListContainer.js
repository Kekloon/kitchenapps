import React, { Component } from 'react'
import { connect } from 'react-redux'
import AddItemComponents from '../components/AddItemComponents'
import MenuListComponents from '../components/MenuListComponents'
import {
    getMenuList,
    // AddItem,
} from '../KitchenAction'

import { Container, Footer, FooterTab } from 'native-base'

class MenuListContainer extends React.Component{
    constructor(props){
        super(props)

        this.state={
            MenuList:[],
            isLoading:false,
           
        }
    }

    componentWillMount(){
         this.props.getMenuList()
    }

    render(){
        return(
            <Container>
                <MenuListComponents
                 MenuList={this.props.MenuList}
                />
           
            </Container> 
        )
    }
}

const mapStateToProps = (state) => ({
    isLoading: state.Kitchen.isLoading,
    MenuList: state.Kitchen.MenuList,
    
})

const mapDispatchToProps=(dispatch)=>({
     getMenuList:()=>{
         dispatch(getMenuList())
     }

  

})

export default connect(mapStateToProps, mapDispatchToProps) (MenuListContainer)