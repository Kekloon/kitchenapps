import { firebase } from '../../firebase.config'

export const getMenuList =()=>(dispatch)=>{
    dispatch(_gettingMenuList())
    firebase.database().ref('/menuList').on('value',(snapshot)=>{
        const data =[];
        snapshot.forEach((child)=>{
            if(child.val())
            data.push({
                name:child.val().name,
                price:child.val().price
            })
        })
    
        dispatch(_SuccessGetMenuList(data))
    }),(e)=>{
        dispatch(_FailedGetMenuList(e))
    }
}

export const AddMenu=(formData)=>(dispatch)=>{
    dispatch(_addingMenuList())
    
    var NewMenu=firebase.database().ref('/menuList/').push();
    // const menuId = NewMenu.key()
    // console.log(menuId)
    // formData = {
    //     ...formData,
    //     menuId
    // }
     NewMenu.set(formData)
    //  .then((res) => {
    //     console.log(res)
    // })
    // .catch (e => {
    //     console.log(e)
    // })
}

export const getOrderList=()=>(dispatch)=>{
    dispatch(_gettingOrderList())
    firebase.database().ref('/OrderList').on('value',snapshot=>{
        const data =[];
        snapshot.forEach((child)=>{
            if(child.val().status==='OnHold')
            data.push({
                name:child.val().name,
                _key:child.key
            })            
        })
        dispatch(_successGetOrderList(data))
    }),(e)=>{
        dispatch(_FailedGetOrderList(e))
    }
}

export const getPreparingList=()=>(dispatch)=>{
    dispatch(_gettingPreparingList())
    firebase.database().ref('/OrderList').on('value',snapshot=>{
        const data =[];
        snapshot.forEach((child)=>{
            if(child.val().status==='Preparing')
            data.push({
                name:child.val().name,
                _key:child.key
            })            
        })
        dispatch(_successGetPreparingList(data))
    }),(e)=>{
        dispatch(_FailedGetPreparingList(e))
    }

}
export const getServedList=()=>(dispatch)=>{
    dispatch(_gettingServedList())
    firebase.database().ref('/OrderList').on('value',snapshot=>{
        const data =[];
        snapshot.forEach((child)=>{
            if(child.val().status==='Served')
            data.push({
                name:child.val().name,
                _key:child.key
            })            
        })
        dispatch(_successGetServedList(data))
    }),(e)=>{
        dispatch(_FailedGetServedList(e))
    }
}

export const changeStatus = (key, status) => (dispatch) => {
    // dispatch(_changingStatus())
    var status1 = firebase.database().ref('/OrderList').child(key);
    status1.update({
        status: 'Preparing'
    })
}

export const changeStatus1 = (key, status) => (dispatch) => {
    // dispatch(_changingStatus())
    var status1 = firebase.database().ref('/OrderList').child(key);
    status1.update({
        status: 'Served'
    })
}



//----------------GET MENU LIST--------------//
const _gettingMenuList=()=>({
    type:'GETTING_MENU_LIST'
})

const _SuccessGetMenuList=(payload)=>({
    type:'SUCCESS_GET_MENU_LIST',
    payload

})

const _FailedGetMenuList=(error)=>({
    type:'Failed_GET_MENU_LIST',
    error

})
//-----------------getting order list----------//
const _gettingOrderList=()=>({
    type:'GETTING_ORDER_LIST'
})

const _successGetOrderList=(payload)=>({
    type:'SUCCESS_GET_ORDER_LIST',
    payload
})

const _FailedGetOrderList=(error)=>({
    type:'FAILED_GET_ORDER_LIST',
    error
})


//-----------------getting peparing list----------//

const _gettingPreparingList=()=>({
    type:'GETTING_PREPARING_LIST',
    
})

const _successGetPreparingList=(payload)=>({
    type:'SUCCESS_GET_PREPARING_LIST',
    payload
})

const _FailedGetPreparingList=(error)=>({
    type:'FAILED_GET_PREPARING_LIST',
    error
})



//-----------------getting served list----------//

const _gettingServedList=()=>({
    type:'GETTING_SERVED_LIST'
})
const _successGetServedList=(payload)=>({
    type:'SUCCESS_GET_SERVED_LIST',
    payload
})

const _FailedGetServedList=(error)=>({
    type:'FAILED_GET_SERVED_LIST',
    error
})


//-----------------add menu list----------------//
const _addingMenuList =()=>(dispatch)=>{
    type:'ADDING_MENU_LIST'
}

const _successAddMenuList = (payload)=>(dispatch)=>{
    type:'SUCCESS_ADD_MENU_LIST'
    payload
}

const _failedAddMenuList =(error)=>(dispatch=>{
    type:'FAILED_ADD_MENU_LIST'
    error
})

// const _changingStatus=()=>{
//     type:'CHANGING_STATUS'
// }