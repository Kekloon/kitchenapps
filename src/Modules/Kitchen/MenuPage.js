import React, { Component } from 'react'
import MenuList from  './container/MenuListContainer'
import AddMenuItem from './container/AddItemContainer'

import { Container,Footer,Button,Text } from 'native-base'

class MenuPage extends Component {
    render(){
        return(
            <Container>      
                 <Button onPress={()=>this.props.navigation.navigate('Main')}>
                 <Text>Order List</Text>
                 </Button>        
                <MenuList/>
            <Footer>
                <AddMenuItem />
            </Footer>    
            </Container>
        )
    }
}

export default MenuPage