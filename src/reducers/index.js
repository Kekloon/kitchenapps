import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";

import Kitchen from "../Modules/Kitchen/KitchenReducer";

export default combineReducers({
	form: formReducer,
	Kitchen,
});
