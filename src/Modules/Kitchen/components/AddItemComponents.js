import React, { Component } from 'react'
import {
    Container,
    Content,
    Item,
    Input,
    Button,
    Text,
    InputGroup
} from 'native-base'
import {StyleSheet}from "react-native";


class AddItemComponents extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id:[],
            name: '',
            price:0, 
        }

        this.clear = this.clear.bind(this);
        this.submit = this.submit.bind(this);
    }

    clear() {
        
        this.setState({name: '', price: ''});
    }

    submit() {
        this.props.AddMenu({
            name: this.state.name,
            price: this.state.price
        }, () => {
            clear();
        })
        // const name = this.state.name.trim();
        // if (name.length) this.props.AddMenu(name);
        // const price =this.state.price.trim();
        // if(price.length)this.props.AddMenu(price);
         

    }

    render() {
        return (
            <Container>
                <Item>
                    <Input style={styles.textColor}
                        placeholder='Add new menu'
                        placeholderTextColor="#000"
                        value={this.state.name}
                        onChangeText={(name) => this.setState({ name })}
                        
                    />
                     <Input style={styles.textColor}
                        keyboardType={'numeric'}
                        placeholder ='Price'
                        placeholderTextColor="#000"
                        value={this.state.price}
                        onChangeText={(price) => this.setState({ price })}
                       
                    />
                    
                    <Button bordered style={styles.buttonContainer}onPress={this.submit}
                    ><Text>Submit</Text></Button>

                </Item>
            </Container>
        )
    }


}

const styles =StyleSheet.create({
    buttonContainer:{
        backgroundColor: "#FBFAFA",
    
    },

    textColor:{
      color:"white",
    },

 
});

export default AddItemComponents