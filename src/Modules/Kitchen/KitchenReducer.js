const initialState = {
    isLoading: false,
    MenuList: [],
    error: null,
    OrderList:[],
    PreparingList:[],
    ServedList:[],
}

function KitchenApp(state = initialState,action ){
    switch(action.type){
        case 'GETTING_MENU_LIST':
        return Object.assign({},state,{
            isLoading:true
        })
    
        case 'SUCCESS_GET_MENU_LIST':
        return Object.assign({},state,{
            isLoading:false,
            MenuList:action.payload

        })
        case 'Failed_GET_MENU_LIST':
        return Object.assign({},state,{
            isLoading:false,
            error:action.payload

        })
//-----------------getting order list----------//
        case 'GETTING_ORDER_LIST':
        return Object.assign({},state,{
            isLoading:true
        })

        case 'SUCCESS_GET_ORDER_LIST':
        return Object.assign({},state,{
            isLoading:false,
            OrderList:action.payload
        })

        case 'FAILED_GET_ORDER_LIST':
        return Object.assign({},state,{
            isLoading:false,
            error:action.payload
        })
        
//-----------------getting peparing list----------//

        case 'GETTING_PREPARING_LIST':
        return Object.assign({},state,{
            isLoading:true
        })

        case 'SUCCESS_GET_PREPARING_LIST':
        return Object.assign({},state,{
            isLoading:false,
            PreparingList:action.payload
        })

        case 'FAILED_GET_PREPARING_LIST':
        return Object.assign({},state,{
            isLoading:false,
            error:action.payload
        })
        
//-----------------getting served list----------//

        case 'GETTING_SERVED_LIST':
        return Object.assign({},state,{
            isLoading:true
        })

        case 'SUCCESS_GET_SERVED_LIST':
        return Object.assign({},state,{
            isLoading:false,
            ServedList:action.payload
        })

        case 'FAILED_GET_SERVED_LIST':
        return Object.assign({},state,{
            isLoading:false,
            error:action.payload
        })

//-----------------add menu list----------//
        case 'ADDING_MENU_LIST':
        return object.assign({},state,{
                isLoading:true

        })

        case 'SUCCESS_ADD_MENU_LIST':
        return object.assign({},state,{
                isLoading:false,
                MenuList:action.payload
        })

        case 'FAILED_ADD_MENU_LIST':
        return object.assign({},state,{
            isLoading: false,
            error: action.error
        })

    }

    return state
}

export default KitchenApp