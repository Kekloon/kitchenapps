import React,{Component} from'react';
import {
    StyleSheet,
    View,
    Text,
    AppRegistry,
    TouchableOpacity,
}from 'react-native';
import { Container, Header, Title, Content, Button, Icon, Left, Right, Body, CheckBox, ListItem, List } from "native-base";

export default class Menu extends React.Component{
    
    render(){
            return(
        <View style={styles.Container}>
        <View style={styles.header}>
            <Text style={styles.headerText}>*Menu list*</Text>    
            <TouchableOpacity style={styles.buttonContainer} onPress={()=>this.props.navigation.navigate('Main')}>
            <Text style={styles.buttonText}>Order List</Text>
            </TouchableOpacity>         
        </View>
        </View>   
        );
    }

}

const styles=StyleSheet.create({
    container:{
        flex:1,
    },
    header:{
        backgroundColor:'#87ceeb',
        justifyContent:'center', 
        alignItems:'center',
    },
    headerText:{
        color:'white',
        fontSize:30,
        padding:26,
        fontWeight:'bold',
    },
    buttonContainer:{
        backgroundColor: '#2980b9',
        width:80,
        height:20,
        alignItems:'center',
        position:'absolute',
        top:40,
        left:-1,
    },
    buttonText:{
        color:'white',
    },
});
