import React,{Component} from 'react'
import {connect}from 'react-redux'
import OrderListComponents from '../components/OrderListComponents'
import{

    getOrderList,
    getPreparingList,
    getServedList,
    changeStatus,
    changeStatus1,

} from '../KitchenAction'
import {Container,Button,Text} from 'native-base'


class OrderListContainer extends Component{
    constructor(props){
    super(props)
        this.state={
            OrderList:[],
            PreparingList:[],
            ServedList:[],
            isLoading:false
        }
    }
//----------------from action---------------//
    componentWillMount(){
        this.props.getOrderList()
        this.props.getPreparingList()
        this.props.getServedList()
    }

    render(){
        return(
            <Container>
                <Button onPress={()=>this.props.navigation.navigate('MenuPage')}>
                <Text>Menu List</Text>
                </Button>
                <OrderListComponents
                // -----------------to components-------------//
                    OrderList={this.props.OrderList}
                    isLoading={this.props.isLoading}
                    PreparingList={this.props.PreparingList}
                    ServedList={this.props.ServedList}
                    changeStatus={this.props.changeStatus}
                    changeStatus1={this.props.changeStatus1}
                 /> 
            </Container>    
        )
    }
}


const mapStateToProps=(state)=>({
    isLoading:state.Kitchen.isLoading,
    OrderList:state.Kitchen.OrderList,
    PreparingList:state.Kitchen.PreparingList,
    ServedList:state.Kitchen.ServedList
})

//---------------To action--------------//
const mapDispatchToProps=(dispatch)=>({
    getOrderList:()=>{
        dispatch(getOrderList())
    },

    getPreparingList:()=>{
        dispatch(getPreparingList())
    },

    getServedList:()=>{
        dispatch(getServedList())
    },

    changeStatus:(key,status)=>{
        dispatch(changeStatus(key,status))
    },

    changeStatus1:(key,status)=>{
        dispatch(changeStatus1(key,status))
    }

})

export default connect(mapStateToProps ,mapDispatchToProps)(OrderListContainer)